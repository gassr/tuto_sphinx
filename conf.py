# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# http://www.sphinx-doc.org/en/master/config
# -- Path setup --------------------------------------------------------------
# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))
import platform
from datetime import datetime
from zoneinfo import ZoneInfo

import sphinx
import sphinx_material

project = "Tuto sphinx CNT-F"
html_title = project

author = f"Scribe CNT-F"
html_logo = "images/cnt.jpg"
html_favicon = "images/cnt.jpg"
release = "0.1.0"
now = datetime.now(tz=ZoneInfo("Europe/Paris"))
version = f"{now.year}-{now.month:02}-{now.day:02} {now.hour:02}H ({now.tzinfo})"
today = version

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.doctest",
    "sphinx.ext.extlinks",
    "sphinx.ext.intersphinx",
    "sphinx.ext.todo",
    "sphinx.ext.mathjax",
    "sphinx.ext.viewcode",
    "myst_parser",
    "sphinx_markdown_tables",
    "sphinx_copybutton",
]
autosummary_generate = True
autoclass_content = "class"

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]
html_static_path = ["_static"]
html_show_sourcelink = True
html_sidebars = {
    "**": ["logo-text.html", "globaltoc.html", "localtoc.html", "searchbox.html"]
}
extensions.append("sphinx_material")
html_theme_path = sphinx_material.html_theme_path()
html_context = sphinx_material.get_html_context()
html_theme = "sphinx_material"

extensions.append("sphinx.ext.intersphinx")
intersphinx_mapping = {
    "congres": ("https://gassr.gitlab.io/congres_confederaux_cntf/", None),
    "syndicats": ("https://gassr.gitlab.io/syndicats_cntf/", None),
    "statuts_conf": ("https://gassr.gitlab.io/statuts_cntf/", None),
    "commissions": ("https://gassr.gitlab.io/commissions_cntf/", None),
    "juridique": ("http://gassr.gitlab.io/juridique/", None),
    "rhone_alpes": ("https://gassr.gitlab.io/rhone_alpes_cntf/", None),
    "paris": ("https://gassr.gitlab.io/iledefrance_cntf/", None),
    "sipmcs": ("https://gassr.gitlab.io/sipmcs_cntf/", None),
    "international": ("https://gassr.gitlab.io/international/", None),
    "livret": ("https://gassr.gitlab.io/livret_accueil_cntf/", None),

    "python": ("https://gdevops.gitlab.io/tuto_python/", None),
    "git": ("https://gdevops.gitlab.io/tuto_git/", None),
    "docu": ("https://gdevops.gitlab.io/tuto_documentation/", None),
    "project": ("https://gdevops.gitlab.io/tuto_project/", None),
    "sphinx_material": ("https://gdevops.gitlab.io/tuto_sphinx_material", None),

}
extensions.append("sphinx.ext.todo")
todo_include_todos = True

# material theme options (see theme.conf for more information)
html_theme_options = {
    "base_url": "https://gassr.gitlab.io/tuto_sphinx",
    "repo_url": "https://gitlab.com/gassr/tuto_sphinx",
    "repo_name": project,
    "html_minify": False,
    "html_prettify": True,
    "css_minify": True,
    "repo_type": "gitlab",
    "globaltoc_depth": -1,
    "color_primary": "black",
    "color_accent": "cyan",
    "touch_icon": "images/cnt.jpg",
    "theme_color": "#2196f3",
    "master_doc": False,
    "nav_title": f"{project} ({today})",
    "nav_links": [
        {
            "href": "https://gdevops.gitlab.io/tuto_sphinx_material/basics.html",
            "internal": False,
            "title": "reStructuredText",
        },
        {
            "href": "https://gassr.gitlab.io/congres_confederaux_cntf/",
            "internal": False,
            "title": "Congres CNT-F",
        },
        {
            "href": "genindex",
            "internal": True,
            "title": "Index",
        },
    ],
    "heroes": {
        "index": "Tuto sphinx CNT-F",
    },
    "table_classes": ["plain"],
}

language = "fr"
html_last_updated_fmt = ""

todo_include_todos = True

html_use_index = True
html_domain_indices = True

copyright = f"2009-{now.year}, {author} Built with sphinx {sphinx.__version__} Python {platform.python_version()}"
