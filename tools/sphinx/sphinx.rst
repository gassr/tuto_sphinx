.. index::
   ! sphinx

.. _tool_sphinx:

====================================================================
**sphinx => un logiciel libre générateur de documentation**
====================================================================

- https://fr.wikipedia.org/wiki/Sphinx_(g%C3%A9n%C3%A9rateur_de_documentation)
- :ref:`docu:documenting_with_sphinx`


Introduction
============

**Sphinx** est **un logiciel libre générateur de documentation**.

Il a été développé par Georg Brandl pour la communauté Python en 2008,
et est le générateur de la documentation officielle de projets tels que
Python, Django, Selenium, Urwid, ou encore Bazaar.

