.. index::
   pair: Image; flamshot
   ! flamshot
   ! Annotation d'images

.. _tool_flamshot:

====================================================================
**Flamshot** pour l'annotation d'images
====================================================================

- https://github.com/lupoDharkael/flameshot


On utilise **flamshot** pour l'annotation d'images.


.. figure:: exemple_annotation.png
   :align: center
