.. index::
   pair: Gimp; Flouter
   pair: Gimp; Rogner
   ! Gimp

.. _tuto_gimp:

====================================================================
**Gimp (GNU IMAGE MANIPULATION PROGRAM)**
====================================================================

- https://www.gimp.org/


Utilisation
============


Copie d'écran
--------------

On utilise gimp pour la copie d'écran.


.. figure::  capture_ecran/capture_ecran.png
   :align: center


.. figure:: capture_ecran/capture_image.png
   :align: center


Redimensionnement des images
================================

On redimensionne les images pour limiter leur poids.

.. figure:: redimensionnement/reduire_taille.png
   :align: center


Rogner la sélection
=======================

Après une copie d'écran on rogne la zone qui nous intéresse
avec l'outil rogner.

.. figure:: rogner/menu_rogner.png
   :align: center


Sélectionner une zone
========================

.. figure:: selectionner/menu_selection_rectangulaire.png
   :align: center


Flouter une zone
==================

Quand on veut cacher un nom ou un visage.

.. figure::  flouter/menu_flou_gaussien.png
   :align: center



