
.. _tool_images:

====================================================================
**Traitement simple d'images**
====================================================================

- https://en.wikipedia.org/wiki/Command-line_interface

.. toctree::
   :maxdepth: 3

   introduction/introduction
   gimp/gimp
   flamshot/flamshot
