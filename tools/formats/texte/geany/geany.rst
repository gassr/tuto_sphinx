.. index::
   pair: Editeur ; geany
   ! geany

====================================================================
**geany un éditeur de texte léger**
====================================================================

- https://www.geany.org/
- https://fr.wikipedia.org/wiki/Geany


Définition
=============

Geany est un éditeur de texte léger utilisant GTK+ et Scintilla et
incluant les fonctions élémentaires d'un environnement de développement
intégré.

Pensé pour avoir peu de dépendances et démarrer rapidement, il est
disponible pour plusieurs systèmes d'exploitation tel que Windows, Linux
Mac OS X3, BSD et Solaris. Il supporte, entre autres, les langages C/C++,
Java, JavaScript, PHP, HTML, CSS, Python, Perl, Ruby, Pascal et Haskell.

Geany est plus puissant que SciTE tout en gardant la simplicité de
celui-ci.
Il n'atteint ni ne vise pour autant la sophistication d'Eclipse.
Il peut remplacer sous Windows des éditeurs tels que NoteTab ou ConTEXT.

C'est un logiciel libre sous licence GNU GPL.

Exemple
============

.. figure:: utilisation_geany.png
   :align: center
