.. index::
   ! Editeurs de texte

.. _tool_editeurs:

====================================================================
**Editeurs de texte**
====================================================================

- https://fr.wikipedia.org/wiki/Environnement_de_d%C3%A9veloppement


.. toctree::
   :maxdepth: 3

   introduction/introduction
   geany/geany
   vscodium/vscodium
