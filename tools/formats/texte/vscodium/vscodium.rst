.. index::
   pair: Editeur ; VSCodium
   ! Renommage en masse
   ! VSCodium

==================================================================================================================
**VSCodium Free/Libre Open Source Software Binaries of VSCode** => pour le renommage en masse dans les fichiers
==================================================================================================================

- https://github.com/VSCodium/vscodium


Introduction
============

VSCodium permet le **renommage en masse** dans un ensemble de ficfhiers.
