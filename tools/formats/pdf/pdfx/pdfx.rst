.. index::
   pair: PDF; pdfx
   ! pdfx

.. _tool_pdfx:

====================================================================
**pdfx**
====================================================================

- https://github.com/metachris/pdfx
- https://www.metachris.com/pdfx/


Reading over this paper and its references recently, I thought it
would be great to be able to download all the references at once…

This inspired me to write a little tool to do just that, and now it’s
done and released under the Apache open source license:

https://github.com/metachris/pdfx

Features
=========


- Extract references and metadata from a given PDF
- Detects pdf, url, arxiv and doi references
- Fast, parallel download of all referenced PDFs
- Find broken hyperlinks (using the -c flag) (more)
- Output as text or JSON (using the -j flag)
- Extract the PDF text (using the --text flag)
- Use as command-line tool or Python package
- Compatible with Python 2 and 3
- Works with local and online pdfs

Extraction de textes
======================

To extract text, you can use the -t flag::

    # Extract text to console
    $ pdfx https://weakdh.org/imperfect-forward-secrecy.pdf -t

::

    # Extract text to file
    $ pdfx https://weakdh.org/imperfect-forward-secrecy.pdf -t -o pdf-text.txt


examine this paper: https://weakdh.org/imperfect-forward-secrecy.pdf
=======================================================================

Lets examine this paper: https://weakdh.org/imperfect-forward-secrecy.pdf

::

    $ pdfx https://weakdh.org/imperfect-forward-secrecy.pdf

::


    Document infos:
    - CreationDate = D:20150821110623-04'00'
    - Creator = LaTeX with hyperref package
    - ModDate = D:20150821110805-04'00'
    - PTEX.Fullbanner = This is pdfTeX, Version 3.1415926-2.5-1.40.14 (TeX Live 2013/Debian) kpathsea version 6.1.1
    - Pages = 13
    - Producer = pdfTeX-1.40.14
    - Title = Imperfect Forward Secrecy: How Diffie-Hellman Fails in Practice
    - Trapped = False

    17 PDF URLs:
    - http://cr.yp.to/factorization/smoothparts-20040510.pdf
    - http://www.spiegel.de/media/media-35671.pdf
    - http://www.spiegel.de/media/media-35529.pdf
    - http://cryptome.org/2013/08/spy-budget-fy13.pdf
    - http://www.spiegel.de/media/media-35514.pdf
    - http://www.spiegel.de/media/media-35509.pdf
    - http://www.spiegel.de/media/media-35515.pdf
    - http://www.spiegel.de/media/media-35533.pdf
    - http://www.spiegel.de/media/media-35519.pdf
    - http://www.spiegel.de/media/media-35522.pdf
    - http://www.spiegel.de/media/media-35513.pdf
    - http://www.spiegel.de/media/media-35528.pdf
    - http://www.spiegel.de/media/media-35526.pdf
    - http://www.spiegel.de/media/media-35517.pdf
    - http://www.spiegel.de/media/media-35527.pdf
    - http://www.spiegel.de/media/media-35520.pdf
    - http://www.spiegel.de/media/media-35551.pdf

Download all referenced pdfs with -d (for download-pdfs) to the specified directory (eg. /tmp/)
==================================================================================================

::

    pdfx https://weakdh.org/imperfect-forward-secrecy.pdf -d ~/tmp

::

    Document infos:
    - CreationDate = D:20150821110623-04'00'
    - Creator = LaTeX with hyperref package
    - ModDate = D:20150821110805-04'00'
    - PTEX.Fullbanner = This is pdfTeX, Version 3.1415926-2.5-1.40.14 (TeX Live 2013/Debian) kpathsea version 6.1.1
    - Pages = 13
    - Producer = pdfTeX-1.40.14
    - Title = Imperfect Forward Secrecy: How Diffie-Hellman Fails in Practice
    - Trapped = False
    - dc = {'format': 'application/pdf', 'description': {'x-default': None}, 'title': {'x-default': 'Imperfect Forward Secrecy: How Diffie-Hellman Fails in Practice'}}
    - pdf = {'Keywords': None, 'Producer': 'pdfTeX-1.40.14', 'Trapped': 'False'}
    - pdfx = {'PTEX.Fullbanner': 'This is pdfTeX, Version 3.1415926-2.5-1.40.14 (TeX Live 2013/Debian) kpathsea version 6.1.1'}
    - rdf = {'li': 'Imperfect Forward Secrecy: How Diffie-Hellman Fails in Practice'}
    - xap = {'CreateDate': '2015-08-21T11:06:23-04:00', 'CreatorTool': 'LaTeX with hyperref package', 'ModifyDate': '2015-08-21T11:08:05-04:00', 'MetadataDate': '2015-08-21T11:08:05-04:00'}
    - xapmm = {'DocumentID': 'uuid:98988d37-b43d-4c1a-965b-988dfb2944b6', 'InstanceID': 'uuid:4e570f88-cd0f-4488-85ad-03f4435a4048'}

    References: 36
    - PDF: 18
    - URL: 18

    PDF References:
    - http://cryptome.org/2013/08/spy-budget-fy13.pdf
    - http://www.spiegel.de/media/media-35526.pdf
    - http://www.spiegel.de/media/media-35551.pdf
    - http://www.spiegel.de/media/media-35529.pdf
    - http://www.spiegel.de/media/media-35513.pdf
    - http://www.spiegel.de/media/media-35528.pdf
    - http://www.hyperelliptic.org/tanja/SHARCS/talks06/thorsten.pdf
    - http://www.spiegel.de/media/media-35517.pdf
    - http://www.spiegel.de/media/media-35519.pdf
    - http://cr.yp.to/factorization/smoothparts-20040510.pdf
    - http://www.spiegel.de/media/media-35671.pdf
    - http://www.spiegel.de/media/media-35509.pdf
    - http://www.spiegel.de/media/media-35514.pdf
    - http://www.spiegel.de/media/media-35520.pdf
    - http://www.spiegel.de/media/media-35533.pdf
    - http://www.spiegel.de/media/media-35522.pdf
    - http://www.spiegel.de/media/media-35527.pdf
    - http://www.spiegel.de/media/media-35515.pdf

    Downloading 18 pdfs to '/home/pvergain/tmp'...
    Error downloading 'http://www.spiegel.de/media/media-35509.pdf' (404)
    Error downloading 'http://www.spiegel.de/media/media-35522.pdf' (404)
    Error downloading 'http://www.spiegel.de/media/media-35533.pdf' (404)
    Error downloading 'http://www.spiegel.de/media/media-35551.pdf' (404)
    Error downloading 'http://www.spiegel.de/media/media-35526.pdf' (404)
    Error downloading 'http://www.spiegel.de/media/media-35527.pdf' (404)
    Downloaded 'http://www.hyperelliptic.org/tanja/SHARCS/talks06/thorsten.pdf' to '/home/pvergain/tmp/imperfect-forward-secrecy.pdf-referenced-pdfs/thorsten.pdf'
    Error downloading 'http://www.spiegel.de/media/media-35529.pdf' (404)
    Error downloading 'http://www.spiegel.de/media/media-35513.pdf' (404)
    Error downloading 'http://www.spiegel.de/media/media-35517.pdf' (404)
    Error downloading 'http://www.spiegel.de/media/media-35515.pdf' (404)
    Error downloading 'http://www.spiegel.de/media/media-35514.pdf' (404)
    Error downloading 'http://www.spiegel.de/media/media-35528.pdf' (404)
    Error downloading 'http://www.spiegel.de/media/media-35519.pdf' (404)
    Error downloading 'http://www.spiegel.de/media/media-35671.pdf' (404)
    Error downloading 'http://www.spiegel.de/media/media-35520.pdf' (404)
    Downloaded 'http://cr.yp.to/factorization/smoothparts-20040510.pdf' to '/home/pvergain/tmp/imperfect-forward-secrecy.pdf-referenced-pdfs/smoothparts-20040510.pdf'
    Downloaded 'http://cryptome.org/2013/08/spy-budget-fy13.pdf' to '/home/pvergain/tmp/imperfect-forward-secrecy.pdf-referenced-pdfs/spy-budget-fy13.pdf'
    All done!
