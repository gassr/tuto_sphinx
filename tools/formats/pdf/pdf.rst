.. index::
   ! PDF (Portable Document Format)

.. _tool_pdf:

====================================================================
Pdf (Portable Document Format)
====================================================================

- https://fr.wikipedia.org/wiki/Portable_Document_Format

.. toctree::
   :maxdepth: 3

   introduction/introduction
   pdfx/pdfx
