
.. _tool_rest:

====================================================================
**Format reStructuredText (RST, ReST, or reST)**
====================================================================

-  :ref:`sphinx_material:ref_rst_cheat_sheet`.

Définition en français
=========================

- https://fr.wikipedia.org/wiki/ReStructuredText


reStructuredText est un langage de balisage léger utilisé notamment dans
la documentation du langage Python.

L'extension associée est parfois indiquée comme étant RST.

L'analyseur syntaxique de référence est implémenté comme élément Docutils
du cadre d'application du langage de programmation Python, mais d'autres
implémentations existent par ailleurs (par exemple Pandoc en Haskell ou
JRst en Java).

**reStructuredText** permet d'exporter dans notamment les formats HTML, XML,
LaTeX, ODF.


Définition en anglais
=========================

- https://en.wikipedia.org/wiki/ReStructuredText

reStructuredText (RST, ReST, or reST) is a file format for textual data
used primarily in the Python programming language community for technical
documentation.

It is part of the Docutils project of the Python Doc-SIG (Documentation
Special Interest Group), aimed at creating a set of tools for Python similar
to Javadoc for Java or Plain Old Documentation (POD) for Perl.

Docutils can extract comments and information from Python programs, and
format them into various forms of program documentation.[1]

In this sense, reStructuredText is a lightweight markup language designed
to be both (a) processable by documentation-processing software such
as Docutils, and (b) easily readable by human programmers who are reading
and writing Python source code.

Syntaxe
=========

Voir l'excellent  :ref:`sphinx_material:ref_rst_cheat_sheet`.

Voir aussi:

- https://restructuredtext.documatt.com/
- http://docutils.sourceforge.net/rst.html
