.. index::
   pair: sphinx ; tools

.. _tuto_formats:

====================================================================
**Formats texte, pdf, images**
====================================================================

.. toctree::
   :maxdepth: 3

   texte/texte
   rest/rest
   pdf/pdf
   images/images
   diagrammes/diagrammes
