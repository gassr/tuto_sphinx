.. index::
   ! gitlab

.. _tuto_sphinx_gitlab:

====================================================================
**gitlab => logiciel libre de forge basé sur git**
====================================================================

- https://fr.wikipedia.org/wiki/GitLab
- https://forum.gitlab.com/

`GitLab <https://fr.wikipedia.org/wiki/GitLab>`_ est un **logiciel libre de forge**
basé sur git proposant les fonctionnalités:

- de wiki,
- un système de suivi des bugs (tickets, issues),
- l’intégration continue et la livraison continue.
