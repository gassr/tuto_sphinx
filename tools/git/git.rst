.. index::
   pair: git ; commandes
   ! git

.. _tuto_sphinx_git:

====================================================================
**git => logiciel libre de gestion de versions décentralisé**
====================================================================

- https://fr.wikipedia.org/wiki/Git
- :ref:`git:git_tuto`


Introduction
============

Git est un logiciel de gestion de versions décentralisé.

C'est un logiciel libre créé par Linus Torvalds, auteur du noyau Linux,
et distribué selon les termes de la licence publique générale GNU version 2.

En 2016, il s’agit du logiciel de gestion de versions le plus populaire
qui est utilisé par plus de douze millions de personnes.


Commandes git
===============

- :ref:`git:git_commands`


Commandes git utilisées sur ce projet
======================================

Rarement (git clone)
------------------------

- git clone (rarement)

Très souvent (**git add, git commit, git push**)
--------------------------------------------------

- git add .
- git commit -m "raison"
- git push

Quelques fois
--------------

- git checkout -- <fichier>

