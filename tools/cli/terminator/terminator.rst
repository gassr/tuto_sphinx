.. index::
   pair: CLI ; Terminator
   ! Terminator

.. _terminator:

====================================================================
**Terminator**
====================================================================

Pour taper les commandes :term:`git` on a besoin d'un terminal.


.. figure:: utiliser_terminator.png
   :align: center
