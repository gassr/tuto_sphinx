.. index::
   ! CLI

.. _tool_cli:

====================================================================
**CLI (Command Line Interface**
====================================================================

- https://en.wikipedia.org/wiki/Command-line_interface

.. toctree::
   :maxdepth: 3

   introduction/introduction
   terminator/terminator
