.. index::
   ! python

.. _tuto_sphinx_python:

====================================================================
**python => Le logiciel sphinx est écrit en Python**
====================================================================

- https://fr.wikipedia.org/wiki/Python_%28langage%29
- :ref:`python:python_tuto`


Introduction
============

Python (prononcé en anglais /ˈpaɪ.θɑn/) est un langage de programmation
interprété, multi-paradigme et multiplateformes.

Il favorise la programmation impérative structurée, fonctionnelle et
orientée objet. Il est doté d'un typage dynamique fort, d'une gestion
automatique de la mémoire par ramasse-miettes et d'un système de gestion
d'exceptions ; il est ainsi similaire à Perl, Ruby, Scheme, Smalltalk et Tcl.

Le logiciel :term:`sphinx` est écrit en Python
