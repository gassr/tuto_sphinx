.. index::
   ! ssh

.. _tool_ssh:

====================================================================
**ssh**
====================================================================


Introduction
============

Pour pouvoir lancer la commande git **git push** sans avoir à écrire
son mot de passe, il faut donner sa clé ssh publique dans l'interface graphique
de gitlab.
