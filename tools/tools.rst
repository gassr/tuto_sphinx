.. index::
   pair: sphinx ; tools

.. _tuto_sphinx_tools:

====================================================================
**Outils pour utiliser sphinx et gitlab**
====================================================================

.. toctree::
   :maxdepth: 3

   cli/cli
   formats/formats
   git/git
   gitlab/gitlab
   python/python
   sphinx/sphinx
   ssh/ssh
   firefox/firefox
