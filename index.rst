
.. figure:: images/bandoconf.jpg
   :align: center


.. figure:: images/repartition_egalitaire_des_richesses.jpg
   :align: center

.. _tuto_sphinx_cnt_f:

====================================================================
**Tuto sphinx/gitlab CNT-F**
====================================================================


.. toctree::
   :maxdepth: 2

   architecture/architecture
   saisie_normale/saisie_normale
   saisie_simple/saisie_simple
   hebergement/hebergement
   tools/tools
   glossaire/glossaire

.. toctree::
   :maxdepth: 1
   :caption: Meta

   meta/meta
