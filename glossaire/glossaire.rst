
.. index::
   pair: glossaire ; Shinx

.. _glossaire_sphinx:

===============
**Glossaire**
===============

.. glossary::
   :sorted:

   forge
   Forge
       - https://fr.wikipedia.org/wiki/Forge_(informatique)

       Une forge permet de rassembler des projets et des développeurs.

       Mais la plupart des forges permettent aussi à des personnes ne
       pratiquant pas la programmation informatique de participer, par
       exemple **les traducteurs ou les graphistes, ou les utilisateurs
       qui s'entraident dans des forums ou soumettent des rapports de
       bogues**.

       Une forge permet donc de rassembler tous ces gens autour du
       développement de projets de logiciel ou **dans notre cas à la CNT-F
       de la documentation**.


   framagit.org
       Instance :term:`gitlab` hébergée en France.

       - https://framagit.org/

   froggit.gr
       instance :term:`gitlab` hébergée en France.

       - http://beta.froggit.fr/

   git
   Git
       :ref:`Git <tuto_sphinx_git>` est un logiciel de gestion de versions décentralisé.

   gitlab
   Gitlab
       - https://fr.wikipedia.org/wiki/GitLab

       GitLab est un logiciel libre de :term:`forge` basé sur git proposant les
       fonctionnalités de wiki, un système de suivi des bugs, l’intégration
       continue et la livraison continue.


   gitlab.com
       instance :term:`gitlab` hébergée aux Etats-Unis.

   hypertextuels
   hyperlien
   hypertexte
       Un hyperlien ou lien hypertexte, est une référence dans un système
       hypertexte permettant de passer automatiquement d'un document
       consulté à un autre document.
       Il a été inventé par Ted Nelson en 1965 dans le cadre du projet Xanadu.

       Depuis les années 1990, les hyperliens sont notamment utilisés
       dans le World Wide Web pour permettre le passage d'une page web
       à une autre.

       - https://fr.wikipedia.org/wiki/Hyperlien

   sphinx
   Sphinx
       :ref:`Sphinx <tool_sphinx>` est **un logiciel libre générateur de documentation**.


