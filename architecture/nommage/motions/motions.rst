.. index::
   pair: Nommage ; Motions

.. _nommage_motions:

====================================================================
**Nommage des motions**
====================================================================


Une motion est décrit dans un fichier qui se nomme de la façon suivante:

- motion_<numero>_<annee>_<nom_du_syndicat>.rst
