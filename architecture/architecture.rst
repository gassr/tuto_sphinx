.. index::
   pair: Architecture ; Fichiers
   ! Architecture

.. _archi_fichiers:

====================================================================
**Architecture des fichiers**
====================================================================

.. toctree::
   :maxdepth: 3

   hierarchie/hierarchie
   nommage/nommage
   intersphinx_mapping/intersphinx_mapping
