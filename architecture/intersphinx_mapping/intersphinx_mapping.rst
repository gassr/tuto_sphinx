.. index::
   pair: Sphinx ; intersphinx_mapping
   ! intersphinx_mapping
   ! Liens hypertextuels entre 2 projets

.. _intersphinx_mapping:

=====================================================================================
**Etablir des liens hypertextuels entre 2 projets => utiliser intersphinx_mapping**
=====================================================================================

- https://fr.wikipedia.org/wiki/Hyperlien

Introduction
==============

**Le grand point fort de sphinx** est de pouvoir établir des liens :term:`hypertextuels`
entre 2 documents appartenant à 2 projets différents.

Pour cela on utilise dans le fichier :ref:`conf.py <fichier_conf_py>`
le dictionnaire intersphinx_mapping.


.. literalinclude:: ../../conf.py
   :lines: 60-80
   :linenos:

Exemple pour le projet ici présent (tuto_sphinx)
===================================================

Lien sur le projet "congres"
---------------------------------


- lien :ref:`sur la motion 24 du congrès 2021 <congres:motion_20_2021_etpic30>`


Lien sur le projet "syndicats"
---------------------------------


- lien :ref:`sur les motions de ETPIC30 <syndicats:motions_etpic30>`


Lien sur le projet "sipmcs"
---------------------------------


- lien :ref:`sur les motions de SIPMCS <sipmcs:motions_sipmcs>`




