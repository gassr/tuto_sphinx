.. index::
   pair: Création; Projet sphinx


.. _creation_projet_sphinx:

================================================
Création d'un projet sphinx (premières étapes)
================================================

- https://gitlab.com/cnt-f/tuto_sphinx/-/commits/main

Création du projet tuto_sphinx
=================================

.. figure:: init_projet/creation_projet.png
   :align: center


Clonage du projet https://gitlab.com/cnt-f/tuto_sphinx
=========================================================

::

    git clone git@gitlab.com:cnt-f/tuto_sphinx.git
    Clonage dans 'tuto_sphinx'...
    warning: Vous semblez avoir cloné un dépôt vide.

Modifier le fichier .git/config
===================================

Modifier le fichier .git/config pour y mettre votre adresse mail.

Example::

    [user]
        name = Noam
        email = noamsw@pm.me


Pour ceux qui ont plusieurs comptes gitlab
================================================================

Créer un fichier ~/.ssh/config avec vos comptes gitlab.com appropriés
---------------------------------------------------------------------

::

    # noamsw@pm.me account
    Host gitlab.com-noamsw
       HostName gitlab.com
       PreferredAuthentications publickey
       IdentityFile ~/.ssh/id_ed25519_noamsw

    # pro account
    Host gitlab.com-pro
       HostName gitlab.com
       PreferredAuthentications publickey
       IdentityFile ~/.ssh/id_ed25519_pro_pm_me

Modifier le fichier .git/config
---------------------------------------------------------------------

En ce qui me concerne je rajoute **-noamsw** à **git@gitlab.com**
car c'est de cette façon que l'ai nommé dans ~/.ssh/config.

::

    [remote "origin"]
        url = git@gitlab.com-noamsw:cnt-f/tuto_sphinx.git


Etat de l'arborescence du projet
==================================

Il n'y a que cette page pour l'instant :)

::

    tuto_sphinx on  master [?]
    ✦ ❯ git status
    Sur la branche master

    Aucun commit

    Fichiers non suivis:
      (utilisez "git add <fichier>..." pour inclure dans ce qui sera validé)

        creation/

    aucune modification ajoutée à la validation mais des fichiers non suivis sont présents (utilisez "git add" pour les suivre)

    tuto_sphinx on  master [?]
    ✦ ❯ tree -L 3
    .
    └── creation
        └── creation.rst

    1 directory, 1 file


Passer de master à main (inutile à partir du 22 mai 2021)
==============================================================

- https://gitlab.com/cnt-f/tuto_sphinx/-/commit/16269d248884bddfc6b70be8b7fa4d954c93f7ff

.. note:: A partir du 22 mai 2021 la branche par défaut sera **main**.

- https://gdevops.gitlab.io/tuto_git/master_to_main/master_to_main.html#operations

On voit qu'on est encore avec une branche master par défaut.
Pas de ça à la CNT-F !

On crée une branche main en suivante ces étapes:

::

    git branch -m master main
    git push -u origin main
    modifier gitlab-ci.yaml
    update the default repository
    delete the master branch

::

    git branch -m master main

::

    git push -u origin main

::


    Total 0 (delta 0), réutilisés 0 (delta 0)
    remote:
    remote: To create a merge request for main, visit:
    remote:   https://gitlab.com/cnt-f/tuto_sphinx/-/merge_requests/new?merge_request%5Bsource_branch%5D=main
    remote:
    To gitlab.com-noamsw:cnt-f/tuto_sphinx.git
     * [new branch]      main -> main
    La branche 'main' est paramétrée pour suivre la branche distante 'main' depuis 'origin'.



Sur https://gitlab.com/cnt-f/tuto_sphinx

.. figure:: main/1_parametres_depot.png
   :align: center

   https://gitlab.com/cnt-f/tuto_sphinx/-/settings/repository


.. figure:: main/2_choisir_branche_main.png
   :align: center


.. figure:: main/3_select_depot_branches.png
   :align: center

   https://gitlab.com/cnt-f/tuto_sphinx/-/branches


.. figure:: main/4_choix_supprimer_master.png
   :align: center

.. figure:: main/5_supprimer_master.png
   :align: center


.. _copier_coller_projet_sphinx:

Copier/coller des fichiers utiles à notre documentation sphinx
===============================================================

- https://gitlab.com/cnt-f/tuto_sphinx/-/commit/a92b4e6201fae3f8efe3beefb9ff35bc51a0d7e8

On ne s'embête pas: un fait un copier/coller des fichiers utiles
à notre documentation sphinx en copiant à partir d'un projet existant
sur son disque local et qui est le clone de https://gitlab.com/cnt-f/congres_confederaux

Copier
--------

.. figure:: template_sphinx/copier_fichiers_congres.png
   :align: center


Coller
---------

.. figure:: template_sphinx/coller_dans_tuto_sphinx.png
   :align: center

Arborescence actuelle
-------------------------

::

    tree -L 3

::

    .
    ├── conf.py
    ├── creation
    │   ├── creation.rst
    │   ├── main
    │   │   ├── 1_parametres_depot.png
    │   │   ├── 2_choisir_branche_main.png
    │   │   ├── 3_select_depot_branches.png
    │   │   ├── 4_choix_supprimer_master.png
    │   │   └── 5_supprimer_master.png
    │   └── template_sphinx
    │       ├── coller_dans_tuto_sphinx.png
    │       └── copier_fichiers_congres.png
    ├── images
    │   ├── bandoconf.jpg
    │   ├── cnt.jpg
    │   ├── favicon.ico
    │   ├── ICL-CIT_Logo.png
    │   └── repartition_egalitaire_des_richesses.jpg
    ├── index.rst
    ├── Makefile
    ├── meta
    │   └── meta.rst
    └── pyproject.toml

    5 directories, 18 files


Modifications des fichiers de base
====================================

Modifications du fichier index.rst
-----------------------------------

- https://gitlab.com/cnt-f/tuto_sphinx/-/commit/108ed2e771f7a50ced5f1aa02ffcfe44af37c88c


Modifications du fichier sphinx **conf.py** (fichier central pour la configuration de sphinx)
--------------------------------------------------------------------------------------------------

https://gitlab.com/cnt-f/tuto_sphinx/-/commit/6cd93aaa1914616bc22c7fc730316bae72d76cc5



Modifications du fichier pyproject.toml (fichier de configuration pour poetry)
-----------------------------------------------------------------------------------

- https://gitlab.com/cnt-f/tuto_sphinx/-/commit/0610e9d64eaea6c6f71921313fcd3eefbe37a788


Modifications du fichier meta/meta.rst
-----------------------------------------

- https://gitlab.com/cnt-f/tuto_sphinx/-/commit/7524702c9c3dfc535a1d7ab73075d438656707b1

Mise à jour des méta-informations.



Ici on suppose que vous avez installé python et poetry de façon correcte
============================================================================

Ici on suppose que vous avez installé python et poetry de façon correcte
Sinon aller au chapitre installation de python et poetry.

Installation de sphinx et des dépendances associés avec poetry
===================================================================


::

    poetry install

::

    Creating virtualenv tuto-sphinx-cnt-vx4WSU6a-py3.9 in /home/noamsw/.cache/pypoetry/virtualenvs
    Updating dependencies
    Resolving dependencies... (11.8s)

    Writing lock file

    Package operations: 40 installs, 0 updates, 0 removals

      • Installing attrs (21.2.0)
      • Installing certifi (2020.12.5)
      • Installing chardet (4.0.0)
      • Installing idna (2.10)
      • Installing markupsafe (2.0.1)
      • Installing pyparsing (2.4.7)
      • Installing pytz (2021.1)
      • Installing urllib3 (1.26.4)
      • Installing alabaster (0.7.12)
      • Installing babel (2.9.1)
      • Installing docutils (0.17.1)
      • Installing imagesize (1.2.0)
      • Installing jinja2 (3.0.1)
      • Installing markdown-it-py (1.1.0)
      • Installing packaging (20.9)
      • Installing pygments (2.9.0)
      • Installing requests (2.25.1)
      • Installing snowballstemmer (2.1.0)
      • Installing soupsieve (2.2.1)
      • Installing sphinxcontrib-applehelp (1.0.2)
      • Installing sphinxcontrib-devhelp (1.0.2)
      • Installing sphinxcontrib-htmlhelp (1.0.3)
      • Installing sphinxcontrib-jsmath (1.0.1)
      • Installing sphinxcontrib-qthelp (1.0.3)
      • Installing sphinxcontrib-serializinghtml (1.1.4)
      • Installing text-unidecode (1.3)
      • Installing unidecode (1.2.0)
      • Installing beautifulsoup4 (4.9.3)
      • Installing css-html-js-minify (2.5.5)
      • Installing lxml (4.6.3)
      • Installing markdown (3.3.4)
      • Installing mdit-py-plugins (0.2.8)
      • Installing python-slugify (5.0.2)
      • Installing pyyaml (5.4.1)
      • Installing sphinx (3.5.3)
      • Installing myst-parser (0.14.0)
      • Installing sphinx-copybutton (0.3.1)
      • Installing sphinx-markdown-tables (0.0.15)
      • Installing sphinx-material (0.0.32+6.g3c3d153 3c3d153)
      • Installing sphinx-panels (0.5.2)


A partir de là la documentation distante a été crée
========================================================

.. figure:: creation_doc/creation_doc_distante.png
   :align: center


On peut aller dans Parametres/Pages pour connaitre l'URL de la documentation


.. figure:: creation_doc/parametres_pages.png
   :align: center


.. figure:: creation_doc/page_deploiement.png
   :align: center

   https://cnt-f.gitlab.io/tuto_sphinx/

.. figure:: creation_doc/tuto_sphinx.png
   :align: center


On peut mettre à jour les paramètres généraux du projet tuto_sphinx
======================================================================

- https://gitlab.com/cnt-f/tuto_sphinx/-/commit/4665a275044d244ee62a860f885cb67825f56e90

.. figure:: parametres_generaux/menu_parametres_general.png
   :align: center


.. figure:: parametres_generaux/parametres_generaux_lien_tuto_sphinx.png
   :align: center

On peut créer la doc en locale
=================================

En fait la plupart du temps la documentation est produite et testée en
local bien entendu.

::

    poetry shell


**make html**  (la commande que vous taperez des milliers de fois)
---------------------------------------------------------------------

::

    make html


Lecture en locale avec firefox
----------------------------------

.. figure:: doc_locale/lecture_doc_locale.png
   :align: center


Ajout des labels Todo et Doing dans gitlab
===========================================

.. figure:: gitlab/ajout_todo_et_doing.png
   :align: center



