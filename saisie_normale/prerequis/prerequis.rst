.. index::
   pair: Prérequis; Projet sphinx


.. _prerequis_projet_sphinx:

=============================================================================
Prérequis pour la production en local d'un projet de documentation sphinx
=============================================================================

Les prérequis sont les suivants::

- installation de git
- installation de python
- installation de poetry
