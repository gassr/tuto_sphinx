.. index::
   ! meta-infos

.. _tuto_sphinx_meta_infos:

=====================
Meta
=====================

Gitlab project
================

- https://gitlab.com/cnt-f/tuto_sphinx


Fichiers sources
================

- https://gitlab.com/cnt-f/tuto_sphinx/-/tree/main

Commits
----------

- https://gitlab.com/cnt-f/tuto_sphinx/-/commits/main


Ticket (Issues en anglais): pour faire des remarques
-----------------------------------------------------

- https://gitlab.com/cnt-f/tuto_sphinx/-/boards

Pipelines
-----------

- https://gitlab.com/cnt-f/tuto_sphinx/-/pipelines


Membres
---------

- https://gitlab.com/cnt-f/tuto_sphinx/-/project_members

pyproject.toml
=================

.. literalinclude:: ../pyproject.toml
   :linenos:


.. _fichier_conf_py:

conf.py
========

.. literalinclude:: ../conf.py
   :linenos:


gitlab-ci.yaml
===============

.. literalinclude:: ../.gitlab-ci.yml
   :linenos:


.pre-commit-config.yaml
========================

.. literalinclude:: ../.pre-commit-config.yaml
   :linenos:


Makefile
==========

.. literalinclude:: ../Makefile
   :linenos:
