.. index::
   pair: Edition ; Web ide
   pair: Sphinx; Gnome

.. _sphinx_modif_simple:

===================================================================================
**Modification simplifiée** (pour les SphinxGnomes, SphinxFees et SphinxFantômes)
===================================================================================

Analogie avec les gnomes de Wikipedia
========================================

- https://fr.wikipedia.org/wiki/Wikip%C3%A9dia:WikiGnome

.. figure:: wiki_gnome.png
   :align: center

Les `WikiGnomes <https://fr.wikipedia.org/wiki/Wikip%C3%A9dia:WikiGnome>`_
travaillent dans l’ombre pour corriger les liens brisés,
retrouver « l’auteur qui a dit ça », et simplement faire mieux fonctionner
les wikis. C'est une espèce très courante, il en existe 1 326.

Autres exemples de comportements typiques du WikiGnome : corriger les
coquilles et les fautes de langue, ajouter des liens ou des catégories
utiles ou encore enrichir d’informations précises un texte vague
(par exemple, en ajoutant « tels que Germinal et L’Assommoir » à la
phrase « Émile Zola a écrit des livres importants »).

Comme les `WikiFées <https://fr.wikipedia.org/wiki/Wikip%C3%A9dia:WikiF%C3%A9e>`_
et les `WikiFantômes <https://fr.wikipedia.org/wiki/Wikip%C3%A9dia:WikiFant%C3%B4me>`_,
les **WikiGnomes sont généralement considérés comme amicaux**.

Les WikiGnomes adorent travailler dans l’ombre, se consacrant aux
modifications mineures, et dans les divers recoins de Wikipédia.
En consultant les modifications récentes, vous pourriez même en voir un
à l’œuvre !

Philosophiquement, ils sont proches des WikiFées, et sont parfois des
WikiGnolls qui se sont assagis.

À l'inverse, **ils n'apprécient pas vraiment les WikiTrolls**.

Introduction
==============

Il est possible d'apporter des modification en éditant les
fichiers texte en utilisant l'éditeur Web (Web IDE).

.. figure:: faq/compte_gitlab/edite_par_aaronswartzeu.png
   :align: center

FAQ
===

.. toctree::
   :maxdepth: 3

   faq/faq
