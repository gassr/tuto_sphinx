

.. _sphinx_saisie_simple:

===================================================================================
FAQ saisie simple
===================================================================================

Je voudrais bien faire une modification mais on me demande de forker le projet, que faire ?
==============================================================================================

.. figure:: compte_gitlab/fork_the_repos.png
   :align: center


Il faut demander une invitation à *l'owner* ou *maintainer* du projet.
Il vous enverra une invitation en tant que **développeur** et vous pourrez
faire des modifications sur le texte d'un fichier.

Exemple les lignes ci-dessous ont été écrites par l'utilisateur @aaronswatrzeu

- https://gitlab.com/cnt-f/tuto_sphinx/-/commit/742d1117c41422a88c4666c67302b68dcab02b24

J'ai reçu une invitation d'un membre 'maintainer' en tant que 'développeur' et
c'est en tant que développeur que j'ai pu écrire ces lignes.


.. figure:: compte_gitlab/edite_par_aaronswartzeu.png
   :align: center


J'ai fait des modifications, elles n'apparaissent pas dans la doc finale, pourquoi ?
======================================================================================

Le mainteneur du projet récupère les modifications grâce à la commande **git pull**
et peut lancer une production de la nouvelle documentation.

