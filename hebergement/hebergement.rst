.. index::
   pair: Hébergement; instance gitlab

.. _tuto_sphinx_hebergement:

====================================================================
**Hébergement de la documentation**
====================================================================

Actuellement sur gitlab.com
===============================

Actuellement (mai 2021) l'hébergement se fait sur **gitlab.com** ce qui nous
procure une maintenance gratuite (mise à jour tous les 22 du mois)

Framagit, froggit, etc.
==========================

Mais l'hébergement peut se faire sur toute autre structure qui utilise
**git** ou **gitlab** par exemple framagit.

Dans ce cas on bénéficie du travail gratuit des admin sys de ces structures.

Framagit
------------

- https://framagit.org/

Froggit
-------

- Rejoins la bêta de #Froggit : une instance #GitLab hébergée en France ➡️ http://beta.froggit.fr/


Sur une machine virtuelle de la CNT-F ?
============================================

Pour être parfaitement autonome, on pourrait installer une machine
virtuelle gitlab dans l'infrastructure de la CNT.

Pour cela il faut **juste** plus d'administrateurs système à la CNT-F :)
